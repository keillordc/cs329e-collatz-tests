#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----
    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_1(self):
        s = "20 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 20)
        self.assertEqual(j, 1000)

    def test_read_2(self):
        s = "1000 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1000)
        self.assertEqual(j, 20)

    def test_read_3(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    # ----
    # cycle_length
    # ----
    def test_cycle_length_1(self):
        cycleLength = cycle_length(1)
        self.assertEqual(cycleLength, 1)

    def test_cycle_length_2(self):
        cycleLength = cycle_length(500003)
        self.assertEqual(cycleLength, 113)

    def test_cycle_length_3(self):
        cycleLength = cycle_length(1000000)
        self.assertEqual(cycleLength, 153)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(9990, 10000)
        self.assertEqual(v, 180)

    def test_eval_6(self):
        v = collatz_eval(50, 59)
        self.assertEqual(v, 113)

    def test_eval_7(self):
        v = collatz_eval(500, 1500)
        self.assertEqual(v, 182)

    def test_eval_8(self):
        v = collatz_eval(999, 1000)
        self.assertEqual(v, 112)

    def test_eval_9(self):
        v = collatz_eval(999999, 1000000)
        self.assertEqual(v, 259)

    def test_eval_10(self):
        v = collatz_eval(555, 724)
        self.assertEqual(v, 171)

    def test_eval_11(self):
        v = collatz_eval(1, 20001)
        self.assertEqual(v, 279)

    def test_eval_12(self):
        v = collatz_eval(3789, 70500)
        self.assertEqual(v, 340)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 100, 110, 114)
        self.assertEqual(w.getvalue(), "100 110 114\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 999, 1000, 112)
        self.assertEqual(w.getvalue(), "999 1000 112\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 999999, 1000000, 259)
        self.assertEqual(w.getvalue(), "999999 1000000 259\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_1(self):
        r = StringIO("50 60\n100 110\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "50 60 113\n100 110 114\n")

    def test_solve_2(self):
        r = StringIO("999 1000\n1000 999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999 1000 112\n1000 999 112\n")

    def test_solve_3(self):
        r = StringIO("999999 1000000\n555 724\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 1000000 259\n555 724 171\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
